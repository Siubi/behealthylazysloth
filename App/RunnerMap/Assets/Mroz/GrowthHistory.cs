﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GrowthHistory
{
    public List<GrowthModifier> GrowthModifiers = new List<GrowthModifier>();
    private enum NextEvent { Modifier, GrowthStop, End }
    public DateTime GlobalGrowthStart => GrowthModifiers[0].GrowthStart;

    public void AddGrowthModifier(DateTime timeStamp, float normalizedIntensity)
    {
        GrowthModifiers.Add(new GrowthModifier(timeStamp, normalizedIntensity));
        GrowthModifiers.OrderBy(x => x.GrowthStart);
    }

    public float Sample(DateTime sampleTime)
    {
        if (GrowthModifiers[0].GrowthStart > sampleTime)
            return 0;
        float result = 0;

        int index = 0;
        while (true)
        {
            var currentModifier = GrowthModifiers[index];
            var nextModifier = GrowthModifiers.Count >= index + 2 ? GrowthModifiers[index + 1] : null;
            var nextModifierTime = nextModifier == null ? sampleTime : sampleTime < nextModifier.GrowthStart ? sampleTime : nextModifier.GrowthStart;


            float growthTime = GrowthModifier.GrowthTime;
            if (nextModifierTime < currentModifier.GrowthEnd)
            {
                //two modifiers in row - no regression
                growthTime = (float)(nextModifierTime - currentModifier.GrowthStart).TotalSeconds;
                result += (growthTime / GrowthModifier.GrowthTime) * currentModifier.Intensity;
            }
            else
            {
                result += currentModifier.Intensity;
                float regressionTime = (float)(nextModifierTime - currentModifier.GrowthEnd).TotalSeconds;
                result -= regressionTime * currentModifier.regressionRate;
            }

            result = Mathf.Clamp(result, 0, 10);

            if (nextModifier == null || nextModifier.GrowthStart > sampleTime)
                return result;
            index++;
        }
    }

    [Serializable]
    public class GrowthModifier
    {
        private static System.Random random = new System.Random();
        public DateTime GrowthEnd;
        public DateTime GrowthStart;
        public static float GrowthTime = 600;
        public static float RegressionTime = 3600 * 24 * 50;
        public float Intensity = 1;
        public GrowthModifier(DateTime timeStamp, float normalizedIntensity)
        {
            GrowthStart = timeStamp;//.AddSeconds((float)random.NextDouble() * GrowthTime * .5f);
            GrowthEnd = GrowthStart.AddSeconds(GrowthTime);
            Intensity = normalizedIntensity;
            //normalizedTime = TimeConverter.ConvertToUnixTimestamp(timeStamp);
        }

        public float regressionRate => 1f / RegressionTime;
    }
}