﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Plant 
{
    private static System.Random random = new System.Random();
    public TrackPoint TPoint;
    public Vector3 WorldPosition;
    public GrowthHistory GrowthHistory = new GrowthHistory();

    public MapOverlayObject MyView { get; set; }

    public float Sample(DateTime timeStamp) => Mathf.Clamp01(GrowthHistory.Sample(timeStamp));
    public DateTime SeedTime => GrowthHistory.GlobalGrowthStart;
    public Plant(TrackPoint trackPosition, float rnd = 0)
    {
        GrowthHistory.AddGrowthModifier(trackPosition.Time, 1);
        WorldPosition = trackPosition.Position;
        WorldPosition += new Vector3((float) random.NextDouble(), 0, (float)random.NextDouble());
        
        //var t = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //t.transform.position = WorldPosition;
        //t.transform.localScale = Vector3.one * .01f;
    }
}


//public static class TimeConverter
//{
//    public static float ConvertToUnixTimestamp(DateTime date)
//    {
//        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
//        TimeSpan diff = date.ToUniversalTime() - origin;
//        return (float) diff.TotalSeconds;
//    }
//}