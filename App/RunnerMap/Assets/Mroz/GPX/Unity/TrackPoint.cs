﻿using Mapbox.Utils;
using System;
using UnityEngine;

public class TrackPoint
{
    public DateTime Time;

    public TrackPoint(Vector3 position, DateTime timeStamp)
    {
        Time = timeStamp;
        Position = position;
    }

    public Vector3 Position { get; internal set; }
}


public static class MapBoxExtensions
{
    public static Vector2 ToUnityVector(this Vector2d x) => new Vector3((float)x.x, 0, (float)x.y);
}