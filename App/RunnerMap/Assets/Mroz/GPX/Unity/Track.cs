﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using Gpx;
using Mapbox.Unity.Utilities;
using UnityEngine;
using System;
using Mapbox.Unity.Map;
using EndomondoExport.GPX;

public class Track
{
    public List<TrackPoint> Run = new List<TrackPoint>();
    public static float step = 4;

    private static AbstractMap _map;

    static AbstractMap Map => _map == null ? _map = GameObject.FindObjectOfType<AbstractMap>() : _map;


    public static float ScaledStep => step * Map.WorldRelativeScale;

    public DateTime StartTime { get; internal set; }
    public DateTime FinishTime { get; internal set; }
    
    
    
    public static Track CreateTrack(trk track)
    {
        if (track.trkseg.Count < 100)
            return null;

        var Run = new List<TrackPoint>();


        var tp1 = track.trkseg[0];
        var p1 = tp1;
        var pos1 = Map.GeoToWorldPosition(new Mapbox.Utils.Vector2d(p1.Latitude, p1.Longitude));

        for (int i = 1; i < track.trkseg.Count - 1; i++)
        {
            var tp2 = track.trkseg[i];
            var p2 = tp2;
            var pos2 = Map.GeoToWorldPosition(new Mapbox.Utils.Vector2d(p2.Latitude, p2.Longitude));

            var dir = pos2 - pos1;
            float distance = dir.magnitude;
            float scaledDistance = distance / Map.WorldRelativeScale;
            if (scaledDistance < 50)
            {

                if (distance > ScaledStep)
                {
                    int nrOfSteps = (int)Mathf.Floor(distance / ScaledStep);

                    var t1 = tp1.Time;
                    var t2 = tp2.Time;
                    long thicksPerStep = (t2.Ticks - t1.Ticks) / nrOfSteps;

                    for (int x = 0; x < nrOfSteps; x++)
                    {
                        var position = pos1 + dir * (float)x / nrOfSteps;
                        var newTP = new TrackPoint(position, new DateTime(t1.Ticks + thicksPerStep * x));
                        Run.Add(newTP);
                    }
                }
                else
                    continue;
            }
            tp1 = tp2;
            p1 = p2;
            pos1 = pos2;
        }


        if (Run.Count < 100)
            return null;

        Track t = new Track();
        t.Run = Run;
        t.StartTime = Run.First().Time;
        t.FinishTime = Run.Last().Time;
        return t;
    }
}