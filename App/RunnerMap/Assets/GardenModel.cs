﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GrowthHistory;

[Serializable]
public class GardenModel
{
    public bool _inited;
    List<DateTime> _timeTable = new List<DateTime>();
    public DateTime GetTime(float progress)
    {
        int index = Mathf.FloorToInt(progress);
        return GetTime(index, progress - index);
    }

    public DateTime GetTime(int index, float progress)
    {
        //if (t < 0)
        //    return _timeTable[0];
        //if (t >= TimeTableLength)
        //    return _timeTable.Last();

        //int k = (int)t;
        //float kk = t - k;



        //if (_timeTable.Count <= k)
        //    return _timeTable.Last();

        var time1 = _timeTable[index];
        var time2 = _timeTable[index + 1];
        

        var time =  new DateTime((long)((time1.Ticks + (time2.Ticks - time1.Ticks) * progress)));
        return time;
    }
    public int TimeTableLength => _timeTable.Count - 1;

    private void InitTimeTable()
    {
        for (int i = 0; i < Tracks.Length; i++)
        {
            _timeTable.Add(Tracks[i].StartTime);
            _timeTable.Add(Tracks[i].FinishTime);//.AddSeconds(GrowthModifier.GrowthTime));
        }
    }


    public Track[] Tracks { get; private set; }
    public List<Plant> Plants { get; private set; } = new List<Plant>();


    public void Init(Track[] tracks)
    {
        Task.Run(() => InitMe(tracks));
    }

    private void InitMe(Track[] tracks)
    {
        Tracks = tracks.OrderBy(x => x.StartTime).ToArray();
        InitTrack(Tracks);
        InitTimeTable();
        _inited = true;
    }

    private void InitTrack(Track[] tracks)
    {
        float maxDst = Track.ScaledStep * 5;

        object _lockObject = new object();
        System.Random rnd = new System.Random();
        foreach (var track in Tracks)
        {
            Parallel.ForEach(track.Run, (trackPosition) =>
            {
                //List<Plant> nearbyPlants = new List<Plant>();
                //float totalAge = 0;
                //foreach (var p in Plants)
                //{
                //    var dst = p.WorldPosition - trackPosition.Position;
                //    if (Mathf.Abs(dst.x) > maxDst ||
                //        Mathf.Abs(dst.y) > maxDst)
                //        continue;

                //    if (dst.magnitude > maxDst)
                //        continue;

                //    if (p.GrowthHistory.GlobalGrowthStart > trackPosition.Time)
                //        continue;

                //    nearbyPlants.Add(p);
                //    p.GrowthHistory.AddGrowthModifier(trackPosition.Time, 1f);
                //    totalAge += p.Sample(trackPosition.Time);
                //}

                if ((float)rnd.NextDouble() < .1f)
                    lock (_lockObject)
                        Plants.Add(new Plant(trackPosition, Track.ScaledStep * 10));
            });
        }
    }
}