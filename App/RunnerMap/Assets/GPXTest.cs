using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EndomondoExport;
using Mapbox.Unity.Map;
using Sirenix.OdinInspector;
using UnityEngine;
using static GrowthHistory;

public class GPXTest : MonoBehaviour
{
    [Range(1, 100)] public float StepSize;
    [Range(1, 100)] public int NrOfTrainings = 20;
    public float GrowintTime = 600;
    public float RegressionTime = 3600 * 24 * 50;

    AbstractMap _map;
    float scale;


    private bool _inited;
    private void Awake()
    {
        _map = FindObjectOfType<AbstractMap>();
        _map.OnInitialized += GPXTest_OnInitialized;
    }

    public void SetTime(float time)
    {
        NormalizedTime = time;
    }

    private void GPXTest_OnInitialized()
    {
        scale = _map.WorldRelativeScale;
        Download();
    }

    GardenModel garden;

    [Range(0, 1)] public float NormalizedTime;


    DateTime time => garden.GetTime(NormalizedTime * garden.TimeTableLength);

    private void Update()
    {
        if (garden == null || !garden._inited)
            return;
        if (!_inited)
            InitObjects();
        gardenView.UpdateForTime(time);
    }


    [ShowInInspector] void PrintTime() => Debug.Log(time);

    [ShowInInspector]
    private void Download()
    {

        EndomondoProxy.instance.downloadingFinishedDelegate += OnFilesDownloadingFinish;
        EndomondoProxy.Authenticate("lukasz@arbuzz.eu", "dupaDupa12", OnLoginFinish);
    }

    private void OnLoginFinish(bool result)
    {
        if (result)
        {
            Debug.Log("Authentication successful, logged in as: " + EndomondoProxy.instance.DisplayName);
            EndomondoProxy.UpdateListOfWorkouts(NrOfTrainings);

        }
        else
        {
            Debug.Log("Login failed");
        }
    }

    private void OnFilesDownloadingFinish()
    {
        Debug.Log("Downloading Finished");
        DoIt();
    }

    [ShowInInspector]
    private void DoIt()
    {
        Track.step = StepSize;
        GrowthModifier.GrowthTime = GrowintTime;
        GrowthModifier.RegressionTime= RegressionTime;
        var t = DateTime.Now;
        var tracks = EndomondoProxy.instance.listOfGPX.Select(x => Track.CreateTrack(x.trk)).Where(x => x != null).ToArray();
        garden = new GardenModel();
        garden.Init(tracks);


    }

    [SerializeField] private GardenView gardenView;

    private void InitObjects()
    {

        gardenView.InitWithModel(garden);
        Debug.Log("XXXXXXXXXXXXXXXXXXXXX");
        _inited = true;
    }
}