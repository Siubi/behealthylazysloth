﻿using System.IO;
using UnityEngine;

public static class FileLoadingService
{
    public static bool FileExists(string fileName)
    {
        return File.Exists(Application.persistentDataPath + "/" + fileName);
    }

    public static void SaveFile(string fileName, string data)
    {
        File.WriteAllText(Application.persistentDataPath + "/" + fileName, data);
    }

    public static string LoadFile(string fileName)
    {
        return File.ReadAllText(Application.persistentDataPath + "/" + fileName);
    }
}
