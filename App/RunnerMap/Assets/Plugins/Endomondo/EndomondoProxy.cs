﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using UnityEngine;
using static EndomondoExport.EndomondoConstants;
using System.Collections;
using UnityEngine.Networking;

namespace EndomondoExport
{
    [Serializable]
    public class EndomondoWorkout
    {
        public string id { get; set; }
        public string start_time { get; set; }
        public string name { get; set; }
        public int sport { get; set; }
        public string steps { get; set; }
        public string distance_km { get; set; }
        public string duration_sec { get; set; }
        public string speed_kmh_avg { get; set; }
        public string route_id { get; set; }
    }

    [Serializable]
    public class WorkoutList
    {
        [SerializeField]
        public EndomondoWorkout[] data { get; set; }
    }

    public class EndomondoProxy : MonoBehaviour
    {
        public string UserId { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AuthToken { get; set; }
        public Dictionary<string, EndomondoWorkout> WorkoutDictionary { get; set; }
        public List<GPX.gpx> listOfGPX { get; set; }

        public delegate void DownloadingFinished();
        public delegate void DownloadingProgress(float progress);
        public DownloadingFinished downloadingFinishedDelegate;
        public DownloadingProgress downloadingProgressDelegate;
        private int numberOfGPXFilesToDownload;

        public bool IsAuthenticated { get { return !string.IsNullOrEmpty(AuthToken); } }

        public EndomondoProxy(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public static EndomondoProxy instance = null;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        public static void Authenticate(string email, string password, Action<bool> callback)
        {
            instance.Email = email;
            instance.Password = password;
            instance.StartCoroutine(AuthenticateCoroutine(callback));
        }

        private static IEnumerator AuthenticateCoroutine(Action<bool> callback)
        {
            bool result = false;
            string authUrl = string.Format("{0}/auth?deviceId=dummy&email={1}&password={2}&country=US&action=PAIR",
                 EndomondoAPIUrl, instance.Email, instance.Password);
            var www = UnityWebRequest.Get(authUrl);
            yield return www.SendWebRequest();

            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.downloadHandler.text))
            {
                string[] lines = www.downloadHandler.text.Split('\n');
                if (lines[0] != "OK")
                {
                    // TODO: Handle Error
                }
                else
                {
                    foreach (string line in lines)
                    {
                        string[] values = line.Split('=');
                        if (values.Length == 2)
                        {
                            switch (values[0])
                            {
                                case "authToken":
                                    instance.AuthToken = values[1];
                                    result = true;
                                    break;
                                case "displayName":
                                    instance.DisplayName = values[1];
                                    break;
                                case "userId":
                                    instance.UserId = values[1];
                                    break;
                                default:
                                    // Nothing
                                    break;
                            }
                        }
                    }
                }
            }
            
            if (callback != null)
            {
                callback.Invoke(result);
            }
        }

        public static void UpdateListOfWorkouts(int maxResults)
        {
            string listUrl = string.Format("{0}/api/workout/list?authToken={1}&maxResults={2}",
                 EndomondoAPIUrl, instance.AuthToken, maxResults);
            instance.StartCoroutine(DownloadData(listUrl, OnListOfWorkoutsReceived));
        }

        private static void OnListOfWorkoutsReceived(string data)
        {
            instance.WorkoutDictionary = new Dictionary<string, EndomondoWorkout>();
            var entries = Mapbox.Json.JsonConvert.DeserializeObject<WorkoutList>(data);
            foreach (var entry in entries.data)
            {
                instance.WorkoutDictionary.Add(entry.id, entry);
            }

            Debug.Log("Downloaded " + entries.data.Length + " workout files");
            UpdateGPXWorkouts(SportType.Running);
        }

        public static void UpdateGPXWorkouts(SportType sportType)
        {
            instance.listOfGPX = new List<GPX.gpx>();
            var gpxFilesToDownload = instance.WorkoutDictionary;//.Where(x => x.Value.sport == (int)sportType);
            instance.numberOfGPXFilesToDownload = gpxFilesToDownload.Count();
            Debug.Log(instance.numberOfGPXFilesToDownload + " gpx files to download"); //of sport type '" + sportType + "' to download");
            foreach (var entry in gpxFilesToDownload)
            {
                if(!FileLoadingService.FileExists(entry.Value.id))
                {
                    string workoutUrl = string.Format("{0}/readTrack?authToken={1}&trackId={2}",
                        EndomondoAPIUrl, instance.AuthToken, entry.Value.id);
                    instance.StartCoroutine(DownloadGPXData(workoutUrl, entry.Value.id, OnGPXWorkoutReceived));
                }
                else
                {
                    OnGPXWorkoutReceived(entry.Value.id, FileLoadingService.LoadFile(entry.Value.id));
                }
            }
        }

        private static void OnGPXWorkoutReceived(string trackID, string data)
        {
            var gpxFormattedData = ParseGPXData(data, trackID);
            if (!FileLoadingService.FileExists(trackID))
            {
                FileLoadingService.SaveFile(trackID, data);
            }
            instance.listOfGPX.Add(gpxFormattedData);
            instance.downloadingProgressDelegate?.Invoke(instance.listOfGPX.Count / instance.numberOfGPXFilesToDownload);
            if (instance.listOfGPX.Count == instance.numberOfGPXFilesToDownload)
            {
                instance.listOfGPX.Sort((x, y) => DateTime.Compare(DateTime.Parse(x.metadata.time), DateTime.Parse(y.metadata.time)));
                Debug.Log("Loaded " + instance.numberOfGPXFilesToDownload + " gpx files");
                instance.downloadingFinishedDelegate?.Invoke();
            }
        }

        private static GPX.gpx ParseGPXData(string dataReceived, string trackID)
        {
            GPX.gpx gpx = null;
            if (!string.IsNullOrEmpty(dataReceived))
            {
                string[] lines = dataReceived.Split('\n');
                if (lines[0] != "OK")
                {
                    // TODO: Error
                }
                else
                {
                    // Info from the activity
                    string[] info = lines[1].Split(';');
                    if (info.Length >= 9)
                    {
                        string[] emailfields = instance.Email.Split('@');
                        gpx = new GPX.gpx()
                        {
                            trackID = trackID,
                            version = "1.1",
                            creator = "Endomondo Export",
                            metadata = new GPX.metadata()
                            {
                                author = new GPX.author()
                                {
                                    email = new GPX.email()
                                    {
                                        id = emailfields[0],
                                        domain = emailfields[1]
                                    },
                                    name = instance.DisplayName
                                },
                                time = UTCTime(info[1])
                            },
                            trk = new GPX.trk()
                            {
                                name = info[4],
                                src = "https://endomondoexport.codeplex.com",
                                type = Enum.GetName(typeof(EndomondoConstants.SportType), int.Parse(info[5])),
                                trkseg = new List<GPX.trkpt>()
                            },
                        };


                        for (int i = 2; i < lines.Length; i++)
                        {
                            string[] values = lines[i].Split(';');
                            if (values.Length >= 9)
                            {
                                double lat;
                                double lon;
                                DateTime time = DateTime.Parse(UTCTime(values[0]));
                                double.TryParse(values[2], out lat);
                                double.TryParse(values[3], out lon);

                                gpx.trk.trkseg.Add(new GPX.trkpt()
                                {
                                    Latitude = lat,
                                    Longitude = lon,
                                    //ele = values[6],
                                    Time = time
                                });
                            }
                        }
                    };
                }
            }
            return gpx;
        }

        private static string UTCTime(string date)
        {
            // 2014-04-05 18:29:30 UTC
            // 2014-04-05T18:29:30Z
            return date.Replace(" UTC", "Z").Replace(" ", "T");
        }

        private static string MakeWebRequest(string url)
        {
            string result = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {

                    Stream receiveStream = response.GetResponseStream();
                    StreamReader readStream = null;
                    if (response.CharacterSet == null)
                        readStream = new StreamReader(receiveStream);
                    else
                        readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                    string data = readStream.ReadToEnd();
                    response.Close();
                    readStream.Close();

                    result = data;
                }
            }
            return result;
        }

        private static IEnumerator DownloadGPXData(string url, string trackID, Action<string, string> callback)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 3f));
            var www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();

            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.downloadHandler.text))
            {
                if (callback != null)
                {
                    callback.Invoke(trackID, www.downloadHandler.text);
                }
            }
        }

        private static IEnumerator DownloadData(string url, Action<string> callback)
        {
            var www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();

            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.downloadHandler.text))
            {
                if (callback != null)
                {
                    callback.Invoke(www.downloadHandler.text);
                }
            }
        }
    }
}