﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EndomondoExport.GPX
{
    public class trkpt
    {
        //[XmlAttribute()] public string lat { get => Latitude.ToString(); set { double.TryParse(value, out Latitude); } }        
        //[XmlAttribute()] public string lon { get => Longitude.ToString(); set { double.TryParse(value, out Longitude); } }

        //public string ele { get; set; }
        [XmlAttribute()] public string time { get => ""; set => DateTime.Parse(value); }

        public DateTime Time;

        public double Latitude;
        public double Longitude;



        // <extensions><gpxtpx:TrackPointExtension><gpxtpx:hr>77</gpxtpx:hr></gpxtpx:TrackPointExtension></extensions>
    }
}