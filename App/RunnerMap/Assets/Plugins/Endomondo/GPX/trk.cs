﻿using System.Collections.Generic;

namespace EndomondoExport.GPX
{
    public class trk
    {
        public string name { get; set; }

        public string src { get; set; }

        public string type { get; set; }

        public List<trkpt> trkseg { get; set; }
    }
}