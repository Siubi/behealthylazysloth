﻿using System;
using System.Linq;
using EndomondoExport;
using Mapbox.Unity.Map;
using UnityEngine;

public class ApplicationController : MonoBehaviour
{
    
    public static ApplicationController Instance;

    private AbstractMap _map;
    private int _currentPartId;
    private float _normalizedTimeInsidePart;
    private bool _isAutoPlay;
    private EndomondoWorkout _currentWorkoutData;
    private GardenModel _garden;
    private bool _inited;
    
    [SerializeField]
    private GardenView gardenView;

    public event Action OnInitialized;
    
    public GardenModel Garden
    {
        get { return _garden; }
        set { _garden = value; }
    }

    public bool IsAutoPlay
    {
        get { return _isAutoPlay; }
    }

    public bool IsNextPartExisting
    {
        get
        {
            if (Garden == null || !Garden._inited)
            {
                return false;
            }
            return _currentPartId+1 < Garden.TimeTableLength;
        }
    }

    public bool IsPreviousPartExisting
    {
        get { return _currentPartId > 0; }
    }

    public EndomondoWorkout CurrentWorkoutData
    {
        get { return _currentWorkoutData; }
    }

    public float NormalizedTimeInsidePart
    {
        get { return _normalizedTimeInsidePart; }
        set { _normalizedTimeInsidePart = value; }
    }

    public void GoToNextPart()
    {
        _currentPartId++;
        _normalizedTimeInsidePart = 0;
        DefineWorkoutData();
    }

    public void JumpToEnd()
    {
        _currentPartId = Garden.TimeTableLength;
        _normalizedTimeInsidePart = 1;
        DefineWorkoutData();
    }

    public void JumpToBegin()
    {
        _currentPartId = 0;
        _normalizedTimeInsidePart = 0;
        DefineWorkoutData();
    }

    public void GoToPreviousPart()
    {
        _currentPartId--;
        _normalizedTimeInsidePart = 0;
        DefineWorkoutData();
    }

    public void GoToAutoPlay(bool play)
    {
        _isAutoPlay = play;
    }
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if(Garden == null || !Garden._inited) { return; }
        
        if (!_inited)
        {
            InitGarden();
        }

        var time = Garden.GetTime(_currentPartId, _normalizedTimeInsidePart);
        gardenView.UpdateForTime(time);

        if(!IsAutoPlay) { return; }

        NormalizedTimeInsidePart += Time.deltaTime * 3;
        if (NormalizedTimeInsidePart >= 1)
        {
            if (IsNextPartExisting)
            {
                GoToNextPart();
            }
            else
            {
                GoToAutoPlay(false);
            }
        }
    }

    private void InitGarden()
    {
        gardenView.InitWithModel(_garden);
        Debug.Log("XXXXXXXXXXXXXXXXXXXXX");
        _inited = true;
        OnInitialized?.Invoke();
    }
    
    private void DefineWorkoutData()
    {
        if (_currentPartId % 2 == 0)
        {
            _currentWorkoutData = EndomondoProxy.instance.WorkoutDictionary[EndomondoProxy.instance.listOfGPX[_currentPartId/2].trackID];
        }
        else
        {
            _currentWorkoutData = null;
        }
    }
}
