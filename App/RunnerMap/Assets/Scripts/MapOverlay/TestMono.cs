﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMono : MonoBehaviour
{
    [SerializeField]
    private float value;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        var matBlock = new MaterialPropertyBlock();
        meshRenderer.GetPropertyBlock(matBlock);
        matBlock.SetFloat("_Intensity", value);
        meshRenderer.SetPropertyBlock(matBlock);
    }
}
