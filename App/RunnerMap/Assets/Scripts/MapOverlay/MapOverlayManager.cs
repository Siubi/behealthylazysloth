﻿using UnityEngine;

public class MapOverlayManager : MonoBehaviour
{
    public static MapOverlayManager Instance;
    
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }
}
