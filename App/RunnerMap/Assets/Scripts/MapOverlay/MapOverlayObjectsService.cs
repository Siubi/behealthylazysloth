﻿using UnityEngine;

public static class MapOverlayObjectsService
{
    private const string CATALOG_FILE_NAME = "MapOverlayObjectsCatalog";

    private static MapOverlayObjectsCatalog _catalog;

    private static MapOverlayObjectsCatalog Catalog
    {
        get
        {
            if (_catalog == null)
            {
                _catalog = Resources.Load<MapOverlayObjectsCatalog>(CATALOG_FILE_NAME);
            }

            return _catalog;
        }
    }
    
    public static MapOverlayObject GetRandomObject(ActivityType activityType)
    {
        var objectsOfType = Catalog.GetMapObjectsOfType(activityType);
        return objectsOfType[Random.Range(0, objectsOfType.Count)];
    }

    public static GameObject GetRandomDecorator()
    {
        return Catalog.Decorators[Random.Range(0, Catalog.Decorators.Count)];
    }
}
