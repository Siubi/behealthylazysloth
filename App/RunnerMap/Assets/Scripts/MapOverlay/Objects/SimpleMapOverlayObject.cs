﻿using DG.Tweening;
using UnityEngine;

public class SimpleMapOverlayObject : MapOverlayObject
{
    [Header("Simple Settings")]
    [SerializeField] private float _minimumScale = 0;
    [SerializeField] private float _maximumScale = 0.01f;
    

    [SerializeField] private Transform view;

    private float _intesity;

    public override void SetIntensity(float intensity)
    {
        if (Mathf.Approximately(_intesity, intensity)) return;
        float deltaIntesity = _intesity - intensity;
        _intesity = intensity;
        base.SetIntensity(intensity);

        var lerpedScale = Mathf.Lerp(_minimumScale, _maximumScale, intensity);
        if (Mathf.Abs(deltaIntesity) > 0.2f)
        {
            transform.DOScale(Vector3.one * lerpedScale, .5f);
            view.DORotate(Vector3.up * (250 * intensity), .5f);
        }
        else
        {
            transform.localScale = Vector3.one * Mathf.Lerp(_minimumScale, _maximumScale, intensity);

        }   
    }
}
