﻿using UnityEngine;

public abstract class MapOverlayObject : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private MapObjectGroup _group;
    [SerializeField] private int _id;

    [SerializeField] protected float decoratorScale = 0.02f;

    private GameObject[] _decorators = new GameObject[1];
    
    public MapObjectGroup Group
    {
        get { return _group; }
    }

    public int Id
    {
        get { return _id; }
    }

    public virtual void SetIntensity(float intensity)
    {
        //var showDecorator = intensity > .1f;
        //for (var i = 0; i < _decorators.Length; i++)
        //{
        //    _decorators[i].SetActive(showDecorator);
        //}
        gameObject.SetActive(intensity > 0f);
    }
    
    public void Init()
    {
        //for (var i = 0; i < 1; i++)
        //{
        //    var randomX = Random.Range(-.5f, .5f);
        //    var randomZ = Random.Range(-.5f, .5f);
        //    var decorator = MapOverlayObjectsService.GetRandomDecorator();
        //    _decorators[i] = Instantiate(decorator, transform.parent);
        //    _decorators[i].transform.localScale = Vector3.one * decoratorScale;
        //    decorator.transform.position = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        //}
    }
}
