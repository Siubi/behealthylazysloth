﻿using UnityEngine;

public class GrassOverlayObject : MonoBehaviour
{
    [SerializeField] private MeshRenderer _meshRenderer;

    private MaterialPropertyBlock _materialPropertyBlock;
    
    private void Start()
    {
        _materialPropertyBlock = new MaterialPropertyBlock();
        UpdateGrass(Random.Range(0f, 0.1f));
    }
    
    
    public void UpdateGrass(float intensity)
    {
        _meshRenderer.GetPropertyBlock(_materialPropertyBlock);
        _materialPropertyBlock.SetFloat("_Intensity", intensity);
        _meshRenderer.SetPropertyBlock(_materialPropertyBlock);
    }
}
