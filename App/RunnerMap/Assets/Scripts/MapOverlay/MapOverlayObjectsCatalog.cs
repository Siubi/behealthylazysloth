﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(menuName = "MapObjects/MapObjectsCatalog")]
public class MapOverlayObjectsCatalog : SerializedScriptableObject
{
    [OdinSerialize]
    private Dictionary<ActivityType, List<MapOverlayObject>> _mapObjects;

    [SerializeField] private List<GameObject> _decorators;

    public Dictionary<ActivityType, List<MapOverlayObject>> AllMapObjects
    {
        get { return _mapObjects; }
    }

    public List<MapOverlayObject> GetMapObjectsOfType(ActivityType activityType)
    {
        return _mapObjects[activityType];
    }

    public List<GameObject> Decorators
    {
        get { return _decorators; }
    }
}
