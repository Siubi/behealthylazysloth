﻿public enum MapObjectGroup
{
    Tree,
    Bush,
    Flower,
    Decoration
}
