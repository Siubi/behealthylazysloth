﻿using System;
using UnityEngine;

public class GardenView : MonoBehaviour
{
    private GardenModel garden;

    public void InitWithModel(GardenModel gardenModel)
    {
        garden = gardenModel;

        foreach (var p in garden.Plants)
        {
            var plant = MapOverlayObjectsService.GetRandomObject(ActivityType.Running);

            var newPlant = Instantiate(plant, transform);
            newPlant.gameObject.SetActive(false);
            newPlant.transform.position = p.WorldPosition;
            newPlant.transform.localScale = Vector3.zero;
            newPlant.gameObject.layer = gameObject.layer;
            newPlant.Init();

            p.MyView = newPlant;
        }
    }

    public void UpdateForTime(DateTime time)
    {
        garden.Plants.ForEach(x => x.MyView.SetIntensity(x.Sample(time)));
    }
}
