﻿using System.Collections;
using System.Collections.Generic;
using EndomondoExport;
using UnityEngine;
using UnityEngine.UI;

public class ControlPanelBehaviour : MonoBehaviour
{
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _prevButton;
    [SerializeField] private Button _jumpNextButton;
    [SerializeField] private Button _jumpPrevButton;
    [SerializeField] private Button _playButton;
    [SerializeField] private Image _playButtonImage;
    [SerializeField] private Slider _slider;
    [SerializeField] private Text _title;
    [SerializeField] private Sprite _playIcon;
    [SerializeField] private Sprite _pauseIcon;

    private void Start()
    {
        _nextButton.onClick.RemoveAllListeners();
        _nextButton.onClick.AddListener(NextClicked);
        _jumpNextButton.onClick.RemoveAllListeners();
        _jumpNextButton.onClick.AddListener(JumpNextClicked);
        _jumpPrevButton.onClick.RemoveAllListeners();
        _jumpPrevButton.onClick.AddListener(JumpPrevClicked);
        _prevButton.onClick.RemoveAllListeners();
        _prevButton.onClick.AddListener(PrevClicked);
        _playButton.onClick.RemoveAllListeners();
        _playButton.onClick.AddListener(PlayClicked);
        _slider.onValueChanged.RemoveAllListeners();
        _slider.onValueChanged.AddListener(SliderMoved);
    }

    private void Update()
    {
        _slider.interactable = !ApplicationController.Instance.IsAutoPlay;
        _slider.value = ApplicationController.Instance.NormalizedTimeInsidePart;

        _nextButton.interactable = _jumpNextButton.interactable = ApplicationController.Instance.IsNextPartExisting;
        _prevButton.interactable = _jumpPrevButton.interactable = ApplicationController.Instance.IsPreviousPartExisting;

        if (ApplicationController.Instance.CurrentWorkoutData == null)
        {
            _title.text = "Break time...";
        }
        else
        {
            var data = ApplicationController.Instance.CurrentWorkoutData;
            _title.text = string.Format("{0} [{1}km]", data.start_time, data.distance_km);
        }
        
        var isPlay = ApplicationController.Instance.IsAutoPlay;
        _playButtonImage.sprite =  isPlay ? _pauseIcon : _playIcon;
    }

    private void SliderMoved(float value)
    {
        if(ApplicationController.Instance.IsAutoPlay) { return; }
        ApplicationController.Instance.NormalizedTimeInsidePart = value;
    }

    private void JumpNextClicked()
    {
        ApplicationController.Instance.JumpToEnd();
    }

    private void JumpPrevClicked()
    {
        ApplicationController.Instance.JumpToBegin();
    }
    
    private void NextClicked()
    {
        ApplicationController.Instance.GoToNextPart();
    }

    private void PrevClicked()
    {
        ApplicationController.Instance.GoToPreviousPart();
    }

    private void PlayClicked()
    {
        ApplicationController.Instance.GoToAutoPlay(!ApplicationController.Instance.IsAutoPlay);
    }
}
