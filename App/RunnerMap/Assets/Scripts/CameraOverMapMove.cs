namespace Mapbox.Examples
{
    using Mapbox.Unity.Map;
    using Mapbox.Unity.Utilities;
    using Mapbox.Utils;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using System;
    using Sirenix.OdinInspector;

    public class CameraOverMapMove : MonoBehaviour
    {
        [SerializeField]
        [Range(1, 100)]
        public float _panSpeed = 1.0f;

        [SerializeField]
        float _zoomSpeed = 0.25f;

        [SerializeField]
        public Camera _referenceCamera;

        [SerializeField]
        AbstractMap _map;

        [SerializeField] private Transform _virtualWorldCamera;
        
        private Vector2d _initialZero;
        private Vector2d _initialOne;
        private Vector3 _initialVirtualCameraPos;

        [ShowInInspector] private float _initialVirtualCameraZoom;

        private Vector3 _origin;
        private Vector3 _mousePosition;
        private Vector3 _mousePositionPrevious;
        private bool _shouldDrag;
        private bool _isInitialized = false;
        private Plane _groundPlane = new Plane(Vector3.up, 0);
        private bool _dragStartedOnUI = false;

        void Awake()
        {
            if (null == _referenceCamera)
            {
                _referenceCamera = GetComponent<Camera>();
                if (null == _referenceCamera) { Debug.LogErrorFormat("{0}: reference camera not set", this.GetType().Name); }
            }
            _map.OnInitialized += () =>
            {
                _isInitialized = true;
                _initialZero = _map.WorldToGeoPosition(Vector3.zero);
                _initialOne = _map.WorldToGeoPosition(Vector3.right);
                _initialVirtualCameraPos = _virtualWorldCamera.transform.position;
                _initialVirtualCameraZoom = _virtualWorldCamera.position.y;
            };
        }

        private void UpdateMap(Vector2d pos, float zoom)
        {
            _map.UpdateMap(pos, zoom);
            Vector3 newZeroPos = _map.GeoToWorldPosition(_initialZero, false);
            Vector3 newOnePos = _map.GeoToWorldPosition(_initialOne, false);
            
            float newZoom = 1f / Vector3.Distance(newOnePos, newZeroPos);

            float newCameraZoom = (_initialVirtualCameraZoom * newZoom);// * Mathf.Cos(angleRadian);

            _virtualWorldCamera.position = _initialVirtualCameraPos - (newZeroPos * newZoom);

            _virtualWorldCamera.position = new Vector3(_virtualWorldCamera.position.x, newCameraZoom, _virtualWorldCamera.position.z);

            // Get lat long at which camera is looking at. 

        }

        public void Update()
        {
            if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject())
            {
                _dragStartedOnUI = true;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _dragStartedOnUI = false;
            }
        }


        private void LateUpdate()
        {
            if (!_isInitialized) { return; }

            if (!_dragStartedOnUI)
            {
                if (Input.touchSupported && Input.touchCount > 0)
                {
                    HandleTouch();
                }
                else
                {
                    HandleMouseAndKeyBoard();
                }
            }
        }

        void HandleMouseAndKeyBoard()
        {
            // zoom
            float scrollDelta = 0.0f;
            scrollDelta = Input.GetAxis("Mouse ScrollWheel");
            ZoomMapUsingTouchOrMouse(scrollDelta);

            //pan mouse
            PanMapUsingTouchOrMouse();
        }

        void HandleTouch()
        {
            float zoomFactor = 0.0f;
            //pinch to zoom.
            switch (Input.touchCount)
            {
                case 1:
                    {
                        PanMapUsingTouchOrMouse();
                    }
                    break;
                case 2:
                    {
                        // Store both touches.
                        Touch touchZero = Input.GetTouch(0);
                        Touch touchOne = Input.GetTouch(1);

                        // Find the position in the previous frame of each touch.
                        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                        // Find the magnitude of the vector (the distance) between the touches in each frame.
                        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                        // Find the difference in the distances between each frame.
                        zoomFactor = 0.01f * (touchDeltaMag - prevTouchDeltaMag);
                    }
                    ZoomMapUsingTouchOrMouse(zoomFactor);
                    break;
                default:
                    break;
            }
        }

        void ZoomMapUsingTouchOrMouse(float zoomFactor)
        {
            var zoom = Mathf.Max(0.0f, Mathf.Min(_map.Zoom + zoomFactor * _zoomSpeed, 21.0f));
            if (Math.Abs(zoom - _map.Zoom) > 0.0f)
            {
                UpdateMap(_map.CenterLatitudeLongitude, zoom);
            }
        }

        private Vector2d _prevCameraWorldPos;
        private float _prevZoom;

        

        void PanMapUsingTouchOrMouse()
        {
            UseMeterConversion();
        }

        void UseMeterConversion()
        {
            if (Input.GetMouseButtonUp(1))
            {
                var mousePosScreen = Input.mousePosition;
                //assign distance of camera to ground plane to z, otherwise ScreenToWorldPoint() will always return the position of the camera
                //http://answers.unity3d.com/answers/599100/view.html
                mousePosScreen.z = _referenceCamera.transform.localPosition.y;
                var pos = _referenceCamera.ScreenToWorldPoint(mousePosScreen);

                var latlongDelta = _map.WorldToGeoPosition(pos);
                Debug.Log("Latitude: " + latlongDelta.x + " Longitude: " + latlongDelta.y);
                //_mapManager.UpdateMap(latlongDelta, _mapManager.Zoom);
            }

            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                var mousePosScreen = Input.mousePosition;
                //assign distance of camera to ground plane to z, otherwise ScreenToWorldPoint() will always return the position of the camera
                //http://answers.unity3d.com/answers/599100/view.html
                mousePosScreen.z = _referenceCamera.transform.localPosition.y;
                _mousePosition = _referenceCamera.ScreenToWorldPoint(mousePosScreen);

                if (_shouldDrag == false)
                {
                    _shouldDrag = true;
                    _origin = _referenceCamera.ScreenToWorldPoint(mousePosScreen);
                }
            }
            else
            {
                _shouldDrag = false;
            }

            if (_shouldDrag == true)
            {
                var changeFromPreviousPosition = _mousePositionPrevious - _mousePosition;
                if (Mathf.Abs(changeFromPreviousPosition.x) > 0.0f || Mathf.Abs(changeFromPreviousPosition.y) > 0.0f)
                {
                    _mousePositionPrevious = _mousePosition;
                    var offset = _origin - _mousePosition;

                    if (Mathf.Abs(offset.x) > 0.0f || Mathf.Abs(offset.z) > 0.0f)
                    {
                        if (null != _map)
                        {
                            float factor = _panSpeed * Conversions.GetTileScaleInMeters((float)0, _map.AbsoluteZoom) / _map.UnityTileSize;
                            var latlongDelta = Conversions.MetersToLatLon(new Vector2d(offset.x * factor, offset.z * factor));
                            //Debug.Log("LatLong Delta : " + latlongDelta);
                            var newLatLong = _map.CenterLatitudeLongitude + latlongDelta;
                            //MapLocationOptions locationOptions = new MapLocationOptions
                            //{
                            //	latitudeLongitude = String.Format("{0},{1}", newLatLong.x, newLatLong.y),
                            //	zoom = _mapManager.Zoom
                            //};
                            UpdateMap(newLatLong, _map.Zoom);
                            //_map.UpdateMap(newLatLong, _map.Zoom);
                        }
                    }
                    _origin = _mousePosition;
                }
                else
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                    {
                        return;
                    }
                    _mousePositionPrevious = _mousePosition;
                    _origin = _mousePosition;
                }
            }
        }
    }
}