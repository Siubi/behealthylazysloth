﻿using EndomondoExport;
using Mapbox.Unity.Map;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static GrowthHistory;

public class UserLoginBehaviour : MonoBehaviour
{
    [SerializeField]
    private InputField emailInputField;
    [SerializeField]
    private InputField passwordInputField;
    [SerializeField]
    private Button loginButton;
    [SerializeField]
    private GameObject wrongLoginPanel;
    [SerializeField]
    private GameObject loadingBarContainer;
    [SerializeField]
    private Image loadingBarFillIn;
    [SerializeField]
    private Animation fadeInPanelAnim;
    [SerializeField]
    private GameObject loginPanelContainer;
    [SerializeField]
    private GameObject controlPanel;

    [Space]
    [Header("Debug")]
    [Range(1, 100)] public float StepSize;
    [Range(1, 100)] public int NrOfTrainings = 20;

    public float GrowintTime = 600;
    public float RegressionTime = 3600 * 24 * 50;

    AbstractMap _map;

    public Transform _parent;
    List<GameObject> _plantsGOs = new List<GameObject>();

    private bool _inited;
    private void Awake()
    {
        _map = FindObjectOfType<AbstractMap>();
        _map.OnInitialized += GPXTest_OnInitialized;
    }

    private void GPXTest_OnInitialized()
    {
        //scale = _map.WorldRelativeScale;
    }

    private float maxLoadingBarFill = 0f;

    private void Start()
    {
        EndomondoProxy.instance.downloadingFinishedDelegate += OnFilesDownloadingFinish;
        EndomondoProxy.instance.downloadingProgressDelegate += OnDownloadingProgress;
        ApplicationController.Instance.OnInitialized += AppInited;
    }

    public void Login()
    {
        loginButton.enabled = false;
        wrongLoginPanel.SetActive(false);
        if (string.IsNullOrEmpty(emailInputField.text) && string.IsNullOrEmpty(passwordInputField.text))
        {
            EndomondoProxy.Authenticate("lukasz@arbuzz.eu", "dupaDupa12", OnLoginFinish);
        }
        else
        {
            EndomondoProxy.Authenticate(emailInputField.text, passwordInputField.text, OnLoginFinish);
        }
    }

    private void OnLoginFinish(bool result)
    {
        if (result)
        {
            Debug.Log("Authentication successful, logged in as: " + EndomondoProxy.instance.DisplayName);
            loadingBarContainer.SetActive(true);
            EndomondoProxy.UpdateListOfWorkouts(NrOfTrainings);
        }
        else
        {
            wrongLoginPanel.SetActive(true);
            loginButton.enabled = true;
            Debug.Log("Login failed");
        }
    }

    private void Update()
    {
        if (loginPanelContainer.activeInHierarchy)
        {
            loadingBarFillIn.fillAmount = Mathf.Lerp(loadingBarFillIn.fillAmount, maxLoadingBarFill, 2f * Time.deltaTime);
        }
    } 

    private void OnDownloadingProgress(float progress)
    {
        maxLoadingBarFill = progress;
    }

    private void OnFilesDownloadingFinish()
    {
        Debug.Log("Downloading Finished");
        Track.step = StepSize;

        GrowthModifier.GrowthTime = GrowintTime;
        GrowthModifier.RegressionTime = RegressionTime;

        var t = DateTime.Now;
        var tracks = EndomondoProxy.instance.listOfGPX.Select(x => Track.CreateTrack(x.trk)).Where(x => x != null).ToArray();
        ApplicationController.Instance.Garden = new GardenModel();
        ApplicationController.Instance.Garden.Init(tracks);
    }

    private void AppInited()
    {
        StartCoroutine(SmoothTransitionWithDelay(2f));
        StartCoroutine(SmoothTransitionWithDelay(2f));
    }
    
    private IEnumerator SmoothTransitionWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        fadeInPanelAnim.Play();
        yield return new WaitForSeconds(fadeInPanelAnim.clip.length / 2);
        controlPanel.SetActive(true);
        loginPanelContainer.SetActive(false);
    }
}
